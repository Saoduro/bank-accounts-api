var billingAccountNumber = context.getVariable("billingAccountNumber");
var correlationId = context.getVariable("correlationId");
var mock = context.getVariable("mock");
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);


var mockResponse = {};
var bankAccount = {};

mockResponse.correlationId = correlationId;

if (mock == "success" || mock === null) {
    mockResponse.billingAccountNumber = billingAccountNumber;
    bankAccount.number = "**********3456";
    bankAccount.nickname = "PM Credit Union";
    bankAccount.routing = "123123123";
    bankAccount.type = "Checking";
    mockResponse.bankAccount = bankAccount;
} else {
    var errors = {};
    var errorList = [];
    var errorListObject = {};

    errorListObject.code = "404";
    errorListObject.message = "Not Found";
    errorListObject.detail = "Could not find the billingAccountNumber: "+ billingAccountNumber;
    errorListObject.timestamp = timeStamp;
    errorList.push(errorListObject);
    errors.code = "404";
    errors.timestamp = timeStamp;
    errors.errorList = errorList;
    mockResponse.errors = errors;
}

context.setVariable("response.content", JSON.stringify(mockResponse));
context.setVariable('response.header.Content-Type', "application/json");