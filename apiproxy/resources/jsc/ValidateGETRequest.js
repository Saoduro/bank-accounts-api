var billingAccountNumber = context.getVariable("request.queryparam.billingAccountNumber");
var mock = context.getVariable("request.queryparam.mock");
context.setVariable("isValidRequest", true);

var regex = new RegExp("^[0-9]{10,}$");

if(billingAccountNumber !== null) {
   if( !regex.test(billingAccountNumber) ) {
        context.setVariable("isValidRequest", false);
        context.setVariable("queryParam", "billingAccountNumber");
        context.setVariable("errorMessage", "Please provide a valid billingAccountNumber : (" + billingAccountNumber + ") it should be numeric with minimum 10 numbers");
    }
} else {
    context.setVariable("isValidRequest", false);
    context.setVariable("queryParam", "billingAccountNumber");
    context.setVariable("errorMessage", "Please provide a valid billingAccountNumber");
} 
if (mock !== null && context.getVariable("isValidRequest") === true) {
    if (mock != "success" && mock != "error" ) {
        context.setVariable("isValidRequest", false);
        context.setVariable("queryParam", "mock");
        context.setVariable("errorMessage", "Please provide the valid mock value");
    }
}

var timeStamp = new Date().toISOString().replace('Z', '');
//context.setVariable("currentSystemTimeStamp", timeStamp);
context.setVariable("mock", mock);
context.setVariable("billingAccountNumber", billingAccountNumber);
context.setVariable("correlationId", context.getVariable("request.queryparam.correlationId"));