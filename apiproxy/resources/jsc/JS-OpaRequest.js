var billingAccountNumber = context.getVariable("request.queryparam.billingAccountNumber");
var headers = {};
headers.authorization = context.getVariable("request.header.authorization");

var http = {};
http.headers = headers;
http.method = context.getVariable("request.verb");
http.billingAccountNumber = billingAccountNumber
http.keyMappingsUrl = "http://"+ context.getVariable("private.keymapping")+"/api/v2/customers/email/keylookup?idtype=EMAIL";

var request = {};
request.http = http;

var attributes = {};
attributes.request = request;

var input = {};
input.attributes = attributes;

var opa_request = {};
opa_request.input = input

context.setVariable("opa_request", JSON.stringify(opa_request));
context.setVariable("data.api.path","/system/authorize_check_digit_account_id");